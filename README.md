# Conversational Development

URL: http://conversationaldevelopment.com

## Running locally

- Install ruby 2.3.0 or greater and the `bundler` gem
- Clone this repo and run `bundle install` in the folder
- Run `bundle exec jekyll serve` and browse to `http://localhost:4000`

## Recently converted to Jekyll

- Jekyll has incrimental builds which when incorporated into CI allows the
  build times to be significantly reduced as the site grows.
- Allows us to compare a Jekyll site vs our Middleman about.gitlab.com site to
  assess the pros and cons.
- Jekyll is the most widely supported and well known static site generator and
  blogging platform (it even runs on Windows with no hassle) allowing others to easily contribute.

# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
