---
---
## Principle 3: Open conversations without consensus

Instead of locking conversation on per-stage, per team, or per-specialty 
principle, leave the doors as open as possible.

<div class='row'>
  <div class='col-md-6'>
    <p>Closed doors:</p>
    <ul>
      <li>SVN(the only way to make a patch was to give someone write access to the repo )</li>
      <li>Not using pull/merge requests</li>
      <li>Each department has its own set of tools</li>
    </ul>
    <img style="width:100%" src="/images/closed-doors.png" />
  </div>
  <div class='col-md-6'>
    <p>Open doors:</p>
    <ul>
      <li>Open repository, documentation &amp; issue tracker</li>
      <li>Chat in shared channels</li>
      <li>Open source parts of your code</li>
    </ul>
    <img style="width:100%" src="/images/open-doors.png" />
  </div>
</div>

### Howto

- Embrace principle of “Written-first online conversations”. Lots of companies 
  might still rely on face-to-face conversations, but that doesn't allow 
  people to contribute.
- Use real-time editing tools like Google Docs for meetings. Everyone can add 
  to agenda, everyone can add to notes, everyone is on the same page, everyone 
  can see what was discussed, immediately see follow-up items.
- Prevent "Not Invented Here" syndrome  by practicing Innersourcing: make it 
  possible for the rest of the organization to contribute.
- If you can make something public, make it public!

### Challenges

People should be able to argue with solutions, but more people and opinions in 
the issue tracker means consensus is harder and no longer a goal.

There are two characteristics of a good conversation: 
**directness and kindness**. Argue with solution, not a person. On the other 
side: you are not your code.

Person that does the work or their boss decides on how open the conversation 
should be. If people don't get used to directness and strive for consensus, 
working out in the open will slow things down .

### Benefits

- Increases reuse of solutions, prevents duplicate work
- Increases shared knowledge
- Ability to scale your organization by going remote &amp; asynchronous

[Principle #4: Result oriented conversations](/results-oriented)
