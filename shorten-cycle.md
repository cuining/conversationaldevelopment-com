---
---
## Principle 1: Shorten the conversation cycle

Start measuring conversation cycle duration from the first discussion of a
feature in chat to actual release in production.

If you can measure the length of separate stages - that is even better.

<div class='row'>
  <div class='col-md-6 col-xs-12'>
    <img style="width:100%" src="/images/shorten1.png" />
  </div>
  <div class='col-md-6 col-xs-12'>
    <img style="width:100%" src="/images/shorten2.png" />
  </div>
</div>


Use these measurements to reduce cycle time. It is a natural thing to do when
you have numbers before the eyes.

### Start working on Minimum Viable Changes

The first thing to consider is to start working on smaller pieces of
functionality. Instead of minimum viable products or minimum viable features,
start thinking in terms of Minimum Viable Changes.

<div class='row'>
  <div class='col-md-2 col-xs-1'>
    &nbsp;
  </div>
  <div class='col-md-8 col-xs-8'>
    <img style="width:100%" src="/images/mvc.png"/>
  </div>
  <div class='col-md-2 col-xs-1'>
    &nbsp;
  </div>
</div>

### Benefits of shipping smaller

<div class='row'>
  <div class='col-md-6 col-xs-6'>
    for business:
    <ul>
      <li>More in line with expectations and needs of stakeholders</li>
      <li>Easier to coordinate and to reason about</li>
      <li>Frequent interactions gather more information</li>
      <li>Quicker to respond to market need</li>
      <li>Higher predictability</li>
    </ul>
  </div>
  <div class='col-md-6 col-xs-6'>
    for developers:
    <ul>
      <li>Prevents overshooting needs and gold plating</li>
      <li>Higher code review quality</li>
      <li>Easier to troubleshoot a small release</li>
      <li>Less time between idea and feedback gives more sense of progress</li>
      <li>Quicker feedback allows you to bind the request and the work to the result</li>
      <li>Faster feedback increases motivation</li>
    </ul>
  </div>
</div>

### Include Gatekeepers into conversation

Think about Gatekeepers who had to approve something before you can ship it:
Security review, Operations department, UX check, Translation department, QA
testing.

<div class='row'>
  <div class='col-md-3 col-xs-1'>
    &nbsp;
  </div>
  <div class='col-md-6 col-xs-6'>
    <img style="width:100%" src="/images/gatekeepers.png" />
  </div>
  <div class='col-md-3 col-xs-1'>
    &nbsp;
  </div>
</div>

We often exclude them from the equation, assuming there’s nothing we can do
about it. But we usually can. Give early access to your code to security
department, involve the UX team throughout the cycle, automate testing and
operations, etc.

### List of changes to think about

- Deploy infrequently → Deploy frequently (if it hurts, do it more often)
- Multiple changes → Single changes (able to link problems to changes quickly)
- Deploy in weekend → Deploy on peak times (prioritize finding problems quickly)
- Human integration testing → Automated integration testing (test everything)

[Principle #2: Thread the conversations through all stages](/thread-conversations)

## FAQ

### What is the wrong way?

Long software development cycles where epics, sprints and story points are the focus.

### What is the correct way?

Shortening the cycle to get an idea into production to bring value to customers as soon as possible.

### What is an example of the correct way?

The complete ConvDev cycle is demonstrated in [GitLab's Idea to Production video](https://www.youtube.com/watch?v=PoBaY_rqeKA).
You can monitor your cycle time using GitLab's Cycle Analytics as shown in the demo.

### What is the benefit to engineers?

Engineers will get to ship more often and see changes in production sooner.

### What is the benefit to managers?

Managers can focus on the immediate most important changes that need to happen.

### What is the benefit to CIOs?

CIOs have more cycles' data to make informed decisions to optimize the software development process.
