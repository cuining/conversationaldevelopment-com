---
sidebar: false
---
## Maturity model

Depending on your business and goals, you can be satisfied with any kind of 
process. It is possible to use FTP for collaboration and deployment and be 
satisfied with it. But obviously the more ambitious your goals are, the more 
efforts you had to put into automation to reach technical excellence.

The table below should help you identify your processes maturity, possible 
problems, and potential for growth:

<table class='table maturity-model-table'>
  <thead>
    <tr>
      <td>
        <strong>Stage</strong>
      </td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Name</th>
      <td>Gatekeeping</td>
      <td>Cross-team</td>
      <td>Integration</td>
      <td>Starting</td>
      <td>Mature</td>
    </tr>
    <tr>
      <th>Typical time for idea to production</th>
      <td>1 year</td>
      <td>1 quarter</td>
      <td>1 month</td>
      <td>1 week</td>
      <td>1 day</td>
    </tr>
    <tr>
      <th>Description</th>
      <td>
        Functional groups are operating independently, Gatekeepers need to 
        sign off for any stage
      </td>
      <td>Conversations happens across teams, SCRUM &amp; Agile</td>
      <td>Teams consist of all relevant people, DevOps</td>
      <td>Complete flow from idea to production</td>
      <td>Teams prioritize and measure speed</td>
    </tr>
    <tr>
      <th>Tools</th>
      <td>SVN, Jira</td>
      <td>Pull&nbsp;Requests(GitHub) or Merge&nbsp;Requests(GitLab)</td>
      <td>CI/CD/Pipelines: Jenkins/GitLab</td>
      <td>Chat/Chatops/IDE</td>
      <td>Dashboard/Integrated set of tools</td>
    </tr>
    <tr>
      <th>Structure</th>
      <td></td>
      <td></td>
      <td>Not measuring from idea yet</td>
      <td></td>
      <td>
        Feedback comes from the systems instead of people. Removal of most 
        meetings
      </td>
    </tr>
    <!-- todo
    <tr>
      <th>Problems</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th>Responsibilities</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    -->
    <tr>
      <th>Failure modes</th>
      <td>
        Many projects are canceled before ever reaching production, Lots of 
        time pinging gatekeepers to sign off, many swim-lanes.
      </td>
      <td>Sprint is holy but what is made is not what is envisioned</td>
      <td>
        Development is fast when it actually starts, long wait before items 
        get scheduled
      </td>
      <td></td>
      <td></td>
    </tr>
    <!-- todo
    <tr>
      <th>What materials are needed to progress to the next stage</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    -->
    <tr>
      <th>Organizations in this stage</th>
      <td></td>
      <td></td>
      <td><a href="https://about.gitlab.com">GitLab Inc.</a></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
