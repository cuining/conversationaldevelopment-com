---
---

{% for post in site.posts %}
  <article>
    <h3 class="title"><a href="{{ post.url }}" class="js-pjax">{{ post.title }}</a></h3>
    <p class="date">{{ post.date | date: "%b %d, %Y" }}</p>
    {{ post.excerpt }}
    <a href="{{ post.url }}">Read more</a> 
  </article>
{% endfor %}
