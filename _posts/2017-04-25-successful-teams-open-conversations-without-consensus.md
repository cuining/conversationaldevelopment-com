---
title: Successful teams open conversation but don't require consensus
date:   2017-04-25 8:00:00 +0100
categories: guide beginner
---

Successful teams have an open and inclusive culture but don’t require consensus to make decisions. 

Instead of locking conversations to a stage, team or specialty leave the doors as open as possible to encourage diverse solutions and improve knowledge sharing. Much like [threading the conversation through all stages](/2017/04/23/thread-the-conversation-through-all-stages/), opening the conversation benefits from a suite of integrated tools and favors a decentralized and asynchronous approach to work which better prepares the team to work independently and even remotely if desired.

<p class="breakout">
An open conversation does not mean everyone must agree before moving forward, it is used to share and compare different opinions and for the manager to make and own the decision on behalf of the team.
</p>

Opening the conversation up can be challenging, it is important that all participants understand and adhere to a code of conduct which at its heart encourages team members to argue the solution with pros and cons and not the person. Agile Coaches can mediate the conversation and Project Managers or Leads can summarize the conversation and make the final decision for the team. 

## How does your team stay focused with open conversations?

Opening a conversation can lead to more talk and less work if not mediated.  We have found that each task or project needs to have someone who is explicitly accountable and owns the decision making process. We would love to know how your team directs conversations to actionable decisions?
