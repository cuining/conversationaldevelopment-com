---
title: Why should you thread the conversation through all stages
date:   2017-04-23 8:00:00 +0100
categories: guide beginner
---

Work consists of many disconnected conversations happening during meetings, phone calls, video conferencing, over email and team messaging or even at lunch or over drink after work. Efficient teams recognize the value of all these conversations and thread them together to create one narrative from idea to production – a single conversation threaded through all mediums of communication.

To thread the conversation through all stages of the software development lifecycle you will need a suite of integrated tools. Automating the the process is the key to making it a success. The more you can remove steps which require team members to manually take information from one tool to another the more knowledge can be shared. 

- Setup your meeting recordings to be automatically uploaded and linked to relevant issues.
- Use ChatOps to quickly and easily create and manage issues in your project management tool directly from your team chat. 
- Integrate your CI/CD pipeline with your code repository and issue tracker to have a complete overview of the health of your project. 

A conversation threaded through all stages will give you traceability allowing you to measure the time and effort required by each team member for every stage enabling you to make changes to improve efficiency.

[Learn how](/2017/04/25/successful-teams-open-conversations-without-consensus/) 
successful teams use Principle 3, 
[open conversations without consensus](/2017/04/25/successful-teams-open-conversations-without-consensus/) 
to get diverse solutions to problems and increase knowledge sharing. 
