---
title: Efficient teams use short conversation cycles to get more done
date:   2017-04-19 8:00:00 +0100
categories: guide beginner
--- 

Efficient teams use shorter conversation cycles by identifying what is the Minimum Viable Change (MVC) that can bring value to customers. They are less concerned by delivering entire products or features in one go and instead ask what is the smallest aspect of the feature or product that can be shipped to bring value to the customer as soon as possible.

<div class='row'>
  <div class='col-md-2 col-xs-1'>
    &nbsp;
  </div>
  <div class='col-md-8 col-xs-8'>
    <img style="width:100%" src="/images/mvc.png"/>
  </div>
  <div class='col-md-2 col-xs-1'>
    &nbsp;
  </div>
</div>

<br>

Interestingly the same rebuttals are always given by those not using MVC, “If we release our product piece by piece our competitors will steal our idea,” or “The product is too complicated and only adds value in its entirety.” 

But using MVC allows you to iterate quicker, receive feedback sooner and more frequently, allowing you to affordably accommodate change and provide truly unique value to your customers while always leaving your competitors a few steps behind your incredible pace.

[Learn how](/2017/04/23/thread-the-conversation-through-all-stages/) 
to use Principle 2 to [thread the conversation through all stages](/2017/04/23/thread-the-conversation-through-all-stages/) to increase transparency and knowledge sharing. 

## We would love to hear from you

How you have shortened your development cycle? What are your thoughts and concerns with implementing shorter cycles?
