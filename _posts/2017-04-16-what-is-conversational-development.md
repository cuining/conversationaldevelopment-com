---
title: What is Conversation Development (ConvDev) and why should you care?
date:   2017-04-16 8:00:00 +0100
categories: guide beginner
---

ConvDev evolves the agile software development principles into a practical framework for modern software development. The agile manifesto and 12 principles are ideals every team should strive for but they don’t tell you how to get there. Frameworks like SCRUM or methodologies like Kanban have attempted to bring more structure and order to the software development process but introduce additional abstractions or endless columns of tasks with not end in sight.

<p class="breakout">
ConvDev takes a different approach by constraining the agile principles to what’s at the center of getting work done, the conversation. 
</p>

No matter what framework or methodology is used, work is always organized around meetings, updates and reviews no matter what department or even industry you are in. 

ConvDev acknowledges this underlying construct of getting work done - the conversation - and instead of hiding it or abstracting it away organizes work around it to create a natural, light and specific result focused framework. The conversation encompasses every stage of taking an idea to production, from discussing what needs can be improved, how to implement it to asking if worked and brought the expected value to the customer.

## The four principles

The values of ConvDev are represented in four principles extracted from years of experience working in, with and observing how efficient and successful teams work.

[Find out how](/2017/04/19/efficient-teams-use-short-conversation-cycles/) 
efficient teams use Principle 1, 
[short conversation cycles](/2017/04/19/efficient-teams-use-short-conversation-cycles/), 
to get more done.
